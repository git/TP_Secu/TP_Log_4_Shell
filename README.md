# TP Log4Shell

## Rappel:

Ce TP a pour but de vous faire découvrir l'exploitation de la vulnérabilité **CVE-2021-44228** qui touche la bibliothèque Log4J. À cause d'elle, un attaquant peut injecter du code dans les logs du serveur qui sera interprété. De plus, l'API **JNDI** sera utilisée. Elle permet aux applications Java de rechercher et d'accéder à des ressources (objets) distantes gérées de manière centralisée via des annuaires, souvent **LDAP**. L'attaquant peut donc, via son injection, rediriger l'application pour récupérer un objet distant via JNDI et l'intégrer à son code. Bien sûr, cette ressource ne sera pas forcément bienveillante envers le serveur hébergeant l'application...



![Alt text](image/schema.png)

<div align="center">fig.1 - Représentation de l'attaque</div>

## Mise en place de l'attaque :

### L'application vulnérable :


Nous allons simuler un simple serveur web qui utilise mal l'API log4j2 et ne vérifie pas les entrée de l'utilisateur avant de les écrire dans les logs du serveur.

Pour simuler ce serveur vulnérable nous vous proposons une simple application web Java. Elle contient simplement une page de login dans laquelle l'utilisateur peut se connecter. 

**Tout d'abord lancez vdn sur le réseau `secure`.**

Vous devrez ensuite <u>**cloner ce dépôt**</u> puis travailler dedans.

Pour déployer cette application nous allons utiliser Docker comme ceci :

```Bash
sudo docker build -t vulnerable-app .
sudo docker run -it --ulimit nofile=122880:122880 --network host vulnerable-app
```

Maintenant l'application tourne en local sur le port **8080**.

<u>Question:</u> :question: 
>En rentrant un identifiant au hasard, assurez vous que le serveur ecrit bien un message d'erreur dans les logs du serveur avec votre identifiant.

Comme vous le savez, l'injection se base sur le fait que quand on attribut à une variable une chaîne de charactère de la forme ${maVaribale}, c'est la valeur de maVariable qui va être attribué.

<u>Question:</u> :question: 

> Testez d'afficher la version de java utiliser par l'application dans les logs du serveur. Pour obtenir la version de java nous utilisons la variable : **java:version**.

Si vous y arrivez, vous comprenez donc bien que **java:version** à été interprété. La faille se base donc sur ce problème, l'attaquant peut donc réaliser ce qu'il veut. Vous vous doutez bien qu'il ne va pas simplement afficher la version de Java...

<u>Question:</u> :question: 

> En cherchant dans le code source de l'application vulnérable, selon vous, quelles lignes de code sont responsables de la vulnérabilité ?


### Votre serveur d'attaquant (LDAP) :

Le but de cette section est de mettre en place un serveur que vous contrôlez en tant qu'attaquant. Ce serveur traite des requêtes JNDI/LDAP et réponds du code malveillant. (voir étapes 2-3 de la fig.1).



Pour créer ce fameux serveur qui vous permettra de donner un script d'infection à utilisé au serveur cible, nous auront besoin d'installer le **java jdk**.

```Bash
    tar -xf jdk-8u20-linux-x64.tar.gz
``` 

<u>Question:</u> :question: 

> Aller dans le fichier qui créé le serveur LDAP et met à disposition le code malveillant si on le requête de la bonne façon. Le **payload** est le bout de code qui permet d'infecté le serveur vulnérable. Selon vous, que fait le payload ici?  (_Nous attendons ici le plus de détails possible_)



Executer cette commande qui va mettre en place le serveur LDAP pret à repondre une requête avec le payload permettant de faire ce que vous avez vu à la question précédente.

```Bash
python3 mechant.py --userip localhost --lport 9001
```

### Préparer la liaison entre le serveur cible et votre machine attaquante :

Pour avoir une interaction avec le serveur infecté, nous devons écouter si d'éventuelles connexions ont lieu depuis celui-ci (voir étape 5 de la fig.1). Pour ce faire utilisez netcat avec les bon arguments. 

<u>Question:</u> :question: 

>Nous voulons que netcat :
> * fonctionne en mode écoute, c'est-à-dire d'attendre une connexion entrante.
> * active le mode verbeux pour afficher des informations détaillées sur les connexions entrantes et sortantes.
> * désactive la résolution DNS
>
>Evidemment, Netcat doit écouter sur le **bon port**.

## Place à l'attaque :

Maintenant que nous sommes prêts, attaquons !


<u>Question:</u> :question: 

> Faite une injection en utilisant jndi qui requêttera votre serveur mis en place précédement qui à l'adresse ... et écoute atuellement sur le port n°.... avec le script /CeQueVousVoulez. Nous n'avons pas besoins de spécifié de scripts précis dans notre cas car notre serveur répond toujours la même chose quand on le requête. 


<u>Question:</u> :question: 

> Que pouvez-vous faire sur le serveur attaqué ? 
> Quels sont vos droits (Oui, cela fait peur...)?

## Questions supplémentaires

Allez sur le site **https://log4shell.tools/** et tester encore une fois la vulnérabilité du serveur cible.

1. Cliquez sur start pour gérer une injection jndi 
2. Injecter la requête jndi dans le login 
3. Revenez sur le site et observez....

